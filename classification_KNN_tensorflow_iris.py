import numpy
from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier

#data form
iris=load_iris()  # get dataset of iris
X=iris.data  # inputs of data (150 x 4)
Y=iris.target  # outputs of data (150 x 1) (index of name)
Z=iris.target_names  # names of class

#train
X_train,X_test,Y_train,Y_test=train_test_split(X,Y)  # split optimaly datas
knn=KNeighborsClassifier(n_neighbors=5)  # create the neural network
knn.fit(X_train,Y_train)  # train the neural network

#test new
X_new=[1,0,1,0.9]  # get new value not in the dataset
prediction_index = knn.predict(numpy.array([X_new]))  # calcul the output
prediction=Z[prediction_index]  # claasified the new value
print(prediction)

#test itself
score=knn.score(X_test,Y_test)  # get score of the neural network
print(score)
