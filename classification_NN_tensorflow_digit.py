import tensorflow as tf
import matplotlib.pyplot as plt
import random
import sys
import numpy as np
import os

mnist = tf.keras.datasets.mnist
(x_train,y_train),(x_test,y_test) = mnist.load_data()

x_train = tf.keras.utils.normalize(x_train,axis=1)
x_test = tf.keras.utils.normalize(x_test,axis=1)

model = tf.keras.models.Sequential()
model.add(tf.keras.layers.Flatten())
model.add(tf.keras.layers.Dense(128,activation=tf.nn.relu))
model.add(tf.keras.layers.Dense(128,activation=tf.nn.relu))
model.add(tf.keras.layers.Dense(10,activation=tf.nn.softmax))

model.compile(
    optimizer='adam',
    loss='sparse_categorical_crossentropy',
    metrics=['accuracy']
)
history = model.fit(
    x_train,
    y_train,
    epochs=5
)
# plt.plot(history.history['accuracy'])
# plt.show()

# dir = sys.path[0]
# path = os.path.join(dir,'test_model.model')
# model.save(path)
# model = tf.keras.models.load_model(path)
val_loss,val_acc = model.evaluate(x_test,y_test)

i = random.randint(0,len(x_test)-1)
x_predict = x_test[i]
y_target = y_test[i]

prediction = model.predict([[x_predict]])
y_predict = np.argmax(prediction)
fiability = prediction[0][y_predict]
print('target: ',y_target)
print('predict: ',y_predict)
print('fiability: ',fiability*100,"%")
for j in range(len(prediction[0])):
    print('prediction ',j,' - ',100*prediction[0][j],'%')
model.summary()
plt.imshow(x_predict)
plt.show()